import './App.css';
import LoginPage from "./components/LoginPage";
import DashboardPage from './components/DashboardPage';
import * as React from "react";
import { useCookies } from "react-cookie";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";


function App() {

  const [cookie, setCookie] = useCookies(["user"]);
  
  function handleLogin(user) {
    setCookie("user", user, { path: "/" });
  }

  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            cookie.user ? (
              <Navigate to="/dashboard" />
            ) : (
              <LoginPage onLogin={handleLogin} />
            )
          }
        />
        <Route path="/dashboard" element={
            cookie.user ? (
              <DashboardPage />
            ) : (
              <Navigate to="/" />
            )
          } 
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
