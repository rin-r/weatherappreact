import * as React from "react";

function TimeClock() {

    var [date,setDate] = React.useState(new Date());
    
    React.useEffect(() => {

        var timer = setInterval(()=>setDate(new Date()), 1000 )
        
        return function cleanup() {
            clearInterval(timer)
        }
    
    });

    return(
        <>{date.toLocaleTimeString()}</>
    )
}

export default TimeClock;