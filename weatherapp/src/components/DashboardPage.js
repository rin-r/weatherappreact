import * as React from "react";
import Header from "./Header";
import WeatherCard from "./WeatherCard";
import TimeClock from "./TimeClock";
import keys from "../keys";
import "./../css/DashboardPage.css"
import FillerElement from "./FillerElement";
const API_KEY = keys.WEATHER_API;

function DashboardPage() {

    const [weatherData, setWeatherData] = React.useState({});
    const [location, setLocation] = React.useState({});

    //Retrieving user location
    React.useEffect(() => {
        navigator.geolocation.getCurrentPosition(
          successCallback,
          errorCallback
        );
    }, []);

    //Fetching weather data on successfull location callback
    const successCallback = (position) => {
        fetch(`https://api.weatherapi.com/v1/forecast.json?key=${API_KEY}&q=${position.coords.latitude},${position.coords.longitude}&days=6&aqi=no&alerts=no`)
          .then((response) => response.json())
          .then((data) => {
            setLocation(data.location);
            setWeatherData(data);
          })
          .catch((error) => {
            console.error("Error fetching weather data:", error);
          });
    };
    
    //Printing error if location callback wasn't successfull
    const errorCallback = (error) => {
        console.log(error);
    };

    return (
        <>
            <Header />
            <main>
                <section className="top-section">
                    <section>
                        <div>
                            <span className="location-city">{location.name}</span>
                            <span className="location-country">{location.country}</span>
                            <br/>
                            <span className="current-time"><TimeClock /></span>
                        </div>
                    </section>
                    {weatherData.current ? (
                            <WeatherCard weatherForecast={{isCurrent: true, data: weatherData.current}}/>
                        ) : (
                            <FillerElement />
                        )
                    }
                </section>
                <section className="other-days-cards">

                {weatherData.forecast && weatherData.forecast.forecastday ? (
                    weatherData.forecast.forecastday.map((day) => (
                    <WeatherCard
                        key={day.date}
                        weatherForecast={{ isCurrent: false, data: day }}
                    />
                    ))
                ) : (
                    <>
                        <FillerElement />
                        <FillerElement />
                        <FillerElement />
                    </>
                )}
                
                </section>
            </main>
        </>
    )
}

export default DashboardPage;