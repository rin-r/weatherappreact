import * as React from "react";
import "./../css/LoginPage.css";
import {useNavigate} from "react-router-dom";

function LoginPanel({onLogin}) {

    const storedEmail = "rin2@gmail.com";
    const storedPassword = "rin2";

    const navigator = useNavigate();

    const [inputs, setInputs] = React.useState({});
    const [inputStatus, setInputStatus] = React.useState({
        userEmail: true,
        userPassword: true,
    });

    //Change state on every change in the inputs
    const onCreditionalsChange = (event) => 
    {
        const elementName = event.target.name;
        const elementValue = event.target.value;

        const isCorrect = elementName == 'userEmail'
            ? elementValue == storedEmail
            : elementValue == storedPassword;
        setInputStatus((status) => ({ ...status, [elementName]: isCorrect }));

        setInputs(data => ({...data, [elementName] : elementValue}));
    }

    //Custom logic on form submit. Checking if user inputs are correct
    const handleSubmit = (event) => 
    {
        event.preventDefault();

        if (inputs.userEmail == storedEmail && inputs.userPassword == storedPassword)
        {
            onLogin({ userEmail: inputs.userEmail, userPassword: inputs.userPassword });
            navigator("/dashboard");
        }
        else 
        {
            alert("Wrong creditinals!");
        }
    }

    return (
        <div className="login-panel-container">
            <span className="title">Login to app</span>
            <form onSubmit={handleSubmit}>
                <div className="input-wrapper">
                    <div className="input-wrapper">
                        <label
                            htmlFor="email"
                        >
                        Email</label>
                        <input 
                            id="email" 
                            name="userEmail" 
                            type="text" 
                            onChange={onCreditionalsChange}
                            value={inputs.userEmail || ""}
                            className={inputStatus.userEmail ? '' : 'wrong'} />
                    </div>
                    <div className="input-wrapper">
                        <label
                            htmlFor="password"
                        >
                        Password</label>
                        <input 
                            id="password" 
                            name="userPassword" 
                            type="password"
                            onChange={onCreditionalsChange}
                            value={inputs.userPassword || ""}
                            className={inputStatus.userPassword ? '' : 'wrong'} />
                    </div>
                </div>
                <div className="inline-wrapper">
                    <input onChange={onCreditionalsChange} id="remember-me" name="userRemember" type="checkbox" />
                    <label
                        htmlFor="remember-me"
                    >
                    Remember me</label>
                </div>
                <button className="login-button" type="submit">Sign in</button>
            </form>
        </div>
    )
}

export default LoginPanel;