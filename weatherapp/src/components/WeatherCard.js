import * as React from "react";

function WeatherCard({weatherForecast}) {

    if (weatherForecast.isCurrent) {

        weatherForecast = weatherForecast.data;

        return (
            <div className="weather-card" >
                <div className="main-box">
                    <span className="name-of-the-day">Current weather</span>
                    <span className="day-date">Last updated: {weatherForecast.last_updated}</span>
                    <span className="weather-status">{weatherForecast.condition.text}</span>
                    <img className="weather-status-icon" src={weatherForecast.condition.icon}></img>
                </div>
                <div className="secondary-box">
                    <div className="column">
                        <span className="weather-data-title">
                            Temperature
                        </span>
                        <span>
                            {weatherForecast.temp_c}°C,
                            feels like {weatherForecast.feelslike_c}°C
                        </span>
                    </div>
                    <div className="column">
                        <span className="weather-data-title">
                            Wind speed
                        </span>
                        <span>
                            {weatherForecast.wind_kph} km/h
                        </span>
                    </div>
                    <div className="column">
                        <span className="weather-data-title">
                            Humidity
                        </span>
                        <span>
                            {weatherForecast.humidity}%
                        </span>
                    </div>
                    <div className="column">
                        <span className="weather-data-title">
                            Wind direction
                        </span>
                        <span>
                            {weatherForecast.wind_dir}
                        </span>
                    </div>
                    <div className="column">
                        <span className="weather-data-title">
                            Pressure
                        </span>
                        <span>
                            {weatherForecast.pressure_mb} mb
                        </span>
                    </div>
                    <div className="column">
                        <span className="weather-data-title">
                            Visibility
                        </span>
                        <span>
                            {weatherForecast.vis_km} km
                        </span>
                    </div>
                </div>
            </div>
        )
    } else {

        weatherForecast = weatherForecast.data;

        let providedDate = new Date(weatherForecast.date);
        let nameOfTheDay = getNameOfTheDay(providedDate);

        return (
            <div className="weather-card" >
                <div className="main-box">
                    <span className="name-of-the-day">{nameOfTheDay}</span>
                    <span className="day-date">{weatherForecast.date}</span>
                    <span className="weather-status">{weatherForecast.day.condition.text}</span>
                    <img className="weather-status-icon" src={weatherForecast.day.condition.icon}></img>
                </div>
                <div className="secondary-box">
                    <div className="inline"><strong>Max t°:</strong> {weatherForecast.day.maxtemp_c}°C</div>
                    <div className="inline"><strong>Min t°:</strong> {weatherForecast.day.mintemp_c}°C</div>
                    <div className="inline"><strong>Average t°:</strong> {weatherForecast.day.avgtemp_c}°C</div>
                    <div className="column">
                        <span className="weather-data-title">
                            Wind speed
                        </span>
                        <span>
                            {weatherForecast.day.maxwind_kph} km/h
                        </span>
                    </div>
                    <div className="column">
                        <span className="weather-data-title">
                            Visibility
                        </span>
                        <span>
                            {weatherForecast.day.avgvis_km} km
                        </span>
                    </div>
                    <div className="column">
                        <span className="weather-data-title">
                            Humidity
                        </span>
                        <span>
                            {weatherForecast.day.avghumidity}%
                        </span>
                    </div>
                    <div className="inline"><strong>Sunrise:</strong> {weatherForecast.astro.sunrise}</div>
                    <div className="inline"><strong>Sunset:</strong> {weatherForecast.astro.sunset}</div>
                    <div className="inline"><strong>Moonrise:</strong> {weatherForecast.astro.moonrise}</div>
                    <div className="inline"><strong>Moonset:</strong> {weatherForecast.astro.moonset}</div>
                </div>
            </div>
        )
    }

    function getNameOfTheDay(providedDate) 
    {
        let currentDate = new Date();

        if (currentDate.toDateString() === providedDate.toDateString())
        {
            return "Today";
        }

        currentDate.setDate(currentDate.getDate() + 1);
        if (currentDate.toDateString() === providedDate.toDateString())
        {
            return "Tommorow";
        }

        return providedDate.toDateString().slice(0, -5);
    }
}

export default WeatherCard;