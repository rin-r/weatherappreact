import * as React from "react";

function FillerElement() {

    return (
        <div className="filler-element">
            <img src="/images/loading.svg"></img>
        </div>
    );
}

export default FillerElement;