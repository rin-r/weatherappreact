import * as React from "react";
import { NavLink, redirect } from "react-router-dom";
import { useCookies } from "react-cookie";


function Header() {

    const [cookie, setCookie, removeCookie] = useCookies(["user"]);

    //Function, that removes cookies and redirects to login page
    function signOut() {
        removeCookie("user");
        redirect("/");
    }

    return (
        <header className="header">
            <section>
                <div className="logo-container">
                    <img className="logo" src="/images/weather-logo.svg"></img>
                    <span className="logo-text">MetaWeather</span>
                </div>
            </section>

            <button id="logOut" onClick={signOut}>Sign out</button>
        </header>
    )
}

export default Header;