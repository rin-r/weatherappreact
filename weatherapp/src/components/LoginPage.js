import * as React from "react";
import LoginPanel from "./LoginPanel";
import "./../css/LoginPage.css";

function LoginPage({onLogin}) {
    return (
        <div className="login-container" >
            <LoginPanel onLogin={onLogin}/>
        </div>
    );
}

export default LoginPage;